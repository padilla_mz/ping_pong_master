class HomeController < ApplicationController
  def index
    @users = User.order(ranking: :desc)
    
  end

  def history
    @games = Game.where(user_id: current_user.id).order(date_payed: :desc)
  end

  def log
    @game_log = Game.new
  end


  def create_log
    game = Game.new(game_params)
    game.user_id = current_user.id

    # Calculates the result of the game
    if game.my_score.to_i > game.opponent_score.to_i
      game.result = 'win'
    else
      game.result = 'lose'
    end

    
    if game.save
      # if game save, modify ranking of players
      opponent = User.find(game.opponent_id.to_i)
      opponent_ranking = opponent.ranking.to_i 
      my_ranking = game.user.ranking.to_i
      game.user.ranking = 1/(1+((10)**((opponent_ranking - my_ranking)/400)))
      opponent.ranking = 1/(1+((10)**((my_ranking - opponent_ranking)/400)))
      game.user.save
      opponent.save

      redirect_to :history
    else
      render :log
      puts game.errors.messages
    end
  end
  private
    def game_params
      params.require(:game).permit(:date_payed, :opponent_id, :my_score, :opponent_score)
    end




end
