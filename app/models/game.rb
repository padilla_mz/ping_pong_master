class Game < ActiveRecord::Base
  belongs_to :user
  validates :date_payed, :result, :opponent_id, :user_id, :opponent_score, :my_score, presence: true

  validate :check_scores


  def check_scores
    score = (opponent_score - my_score)
    puts score = score.abs

    errors.add(:opponent_score, "The score can't be equal") if opponent_score == my_score
    errors.add(:result, "The win score has to be grater than 2") unless score > 2
  end
end
