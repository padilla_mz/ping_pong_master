class ModifyGameFields < ActiveRecord::Migration
  def change
    remove_column :games, :opponent

    add_reference :games, :opponent, index: true
    add_column :users, :ranking, :integer
    
  end
end
