require 'test_helper'

class GameTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "should not save the game" do
    game = Game.new
    assert_not game.save
  end

  test "should save the game log" do 
    game = Game.new
    game.date_payed = "2018-06-07 18:25:54"
    game.opponent_id = 3
    game.opponent_score = 19
    game.my_score = 21
    game.user_id = 1
    game.result = "win"

    assert game.save
  end
end
