Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"
  get '/history', to: 'home#history'
  post '/create_log', to: "home#create_log"
  get '/log', to: 'home#log'
end
