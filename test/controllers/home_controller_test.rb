require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should redirect if get index and logged out" do
    get :index
    assert_response :redirect
  end

  test "should get index if logged in" do
    user = users(:default)
    sign_in user

    get :index
    assert_response :success
  end

  test "should redirect if on log_game when not signed_in" do
    get :log
    assert_response :redirect
  end

  test "Should create a game log" do 
    user = users(:default)
    sign_in user

    get :index
    assert_response :success
  end

  test "should redirect on history when not signed_in" do
    get :history
    assert_response :redirect
  end

  test "Should create a game log" do 
    user = users(:default)
    sign_in user

    get :history
    assert_response :success
  end

  test "should create a game log" do
    user = users(:default)
    sign_in user

    post :create_log, params: {game: {date_payed: "2018-06-07 18:25:54", opponent_id: 1, user_id: 2, my_score: 21, opponent_score: 19}}
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test "should NOT create a game log" do
    user = users(:default)
    sign_in user

    post :create_log, params: {game: {date_payed: "2018-06-07 18:25:54", opponent_id: 1, user_id: 2, my_score: 21, opponent_score: 21}}
    assert_response :error
  end
end
