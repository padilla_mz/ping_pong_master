class MakeChangesToTheGamesSchema < ActiveRecord::Migration
  def change
    remove_column :games, :player_1_id
    remove_column :games, :player_2_id
    remove_column :games, :player_1_score
    remove_column :games, :player_2_score

    add_column :games, :opponent, :string
    add_reference :games, :user, index: true
    add_column :games, :opponent_score, :integer
    add_column :games, :my_score, :integer
  end

end
